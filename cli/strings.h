//
// Created by rakurs on 5/1/24.
//

#ifndef OSSA_STRINGS_H
#define OSSA_STRINGS_H

const char *sym_names[] = {
        "Unknown",
        "Char",
        "Unsigned char",
        "Entity",
        "Integer",
        "unsigned integer",
        "String",
        "Handler for events",
        "Function",
        "Chat",
        "Plugin",
        "Structure ",
        "",
        "",
        "",
        "",
        "Array of ",
        "Type "
};
struct __STR_RES{
    char *settings_class;
    char *settings_var;
    char *exit_command;
    char *envinfo_command;
    char *mkchat_command;
    char *funcStructure;
    char *lschats;
    char *lsplugin;
}defDefs = {
        "{\"name\":\"struct.settings\",\"type\":\"struct\",\"members\":[{\"name\":\"defaultUsercomsLocation\",\"type\":\"string\"},{\"name\":\"config\",\"type\":\"string\"},{\"name\":\"plugins\",\"type\":\"string\"},{\"name\":\"defaultPlugmanLocation\",\"type\":\"string\"},{\"name\":\"flags\",\"type\":\"uint\"}]}",
        "{\"name\": \"settings\",\"type\":\"struct.settings\"}",
        "{\"name\":\"exit\",\"type\":\"function\",\"arguments\":[]}",
        "{\"name\":\"envinfo\",\"type\":\"function\",\"arguments\":[]}",
        "{\"name\":\"mkchat\",\"type\":\"function\",\"arguments\":[{\"name\":\"chatName\",\"type\":\"string\"},{\"name\":\"pluginName\",\"type\":\"string\"}]}",
        "{\"name\":\"fnsyntax\",\"type\":\"function\",\"arguments\":[{\"name\":\"fnname\",\"type\":\"string\"}]}",
        "{\"name\":\"lschats\",\"type\":\"function\",\"arguments\":[]}",
        "{\"name\":\"lsplugin\",\"type\":\"function\",\"arguments\":[]}"
};

#endif //OSSA_STRINGS_H
