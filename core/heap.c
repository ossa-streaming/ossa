//
// Created by rakurs on 5/1/24.
//

#include "./heap.h"
#include "./ossaconsts.h"
#include "./types.h"
#include "./elfloader/elf_loader.h"
#include "./parson/parson.h"

struct ossaSymbol *heap;

extern void *getHeapPointer(){
    return heap;
}
/* local */
extern int pushToHeap(char type, char *symname, void *data){
    exportToHeap(0x0, type, symname, data);
}
struct ossaSymbol compileType(char *description, void *exec){
    struct ossaSymbol sym;
    sym.type = OSSA_SYM_TYPE;
    sym.next = 0x0;
    struct json_object_t *descr = json_value_get_object(json_parse_string(description));
    sym.name = json_object_get_string(descr, "name");
    char *injson_type = json_object_get_string(descr, "type");
    if(!strcmp("struct", injson_type)){
        sym.type |= OSSA_SYM_STRUCTURE;
        struct json_array_t *members = json_object_get_array(descr, "members");
        struct ossaSymbol *fmember = malloc(sizeof(struct ossaSymbol)+sizeof (void*)),
                *pmember = fmember;
        for(int i = 0; i < json_array_get_count(members); i++) {
            *pmember = compileType(json_serialize_to_string(json_array_get_value(members, i)),0x0);
            pmember->next = 0x0;
            if(i+1 < json_array_get_count(members)){
                pmember->next = malloc(sizeof(struct ossaSymbol));
                pmember = pmember->next;
            }
        }
        sym.data = fmember;
        //hidden exec
        void **hexec = (sym.data+sizeof(struct ossaSymbol));
        *hexec = exec;
    }else if(!strcmp("function", injson_type)){
        sym.type = OSSA_SYM_FUNC;
        struct json_array_t *members = json_object_get_array(descr, "arguments");
        struct ossaSymbol *fmember = malloc(sizeof(struct ossaSymbol)+sizeof (void*)),
                *pmember = fmember;
        for(int i = 0; i < json_array_get_count(members); i++) {
            *pmember = compileType(json_serialize_to_string(json_array_get_value(members, i)),0x0);
            pmember->next = 0x0;
            if(i+1 < json_array_get_count(members)){
                pmember->next = malloc(sizeof(struct ossaSymbol));
                pmember = pmember->next;
            }
        }
        sym.data = fmember;
        //hidden exec
        void **hexec = (sym.data+sizeof(struct ossaSymbol));
        *hexec = exec;
    }
    else{
        sym.data = 0x0;
        sym.next = 0x0;
        sym.type = 0;
        if(!strcmp("char", injson_type))
            sym.type |= OSSA_SYM_CHAR;
        else if(!strcmp("uchar", injson_type))
            sym.type |= OSSA_SYM_UCHAR;
        else if(!strcmp("entity", injson_type))
            sym.type |= OSSA_SYM_ENTITY;
        else if(!strcmp("int", injson_type))
            sym.type |= OSSA_SYM_INT;
        else if(!strcmp("uint", injson_type))
            sym.type |= OSSA_SYM_UINT;
        else if(!strcmp("string", injson_type))
            sym.type |= OSSA_SYM_STR;
        else if(!strcmp("handler", injson_type))
            sym.type |= OSSA_SYM_HANDLER;
        else if(!strcmp("function", injson_type))
            sym.type |= OSSA_SYM_FUNC;
        else if(!strcmp("chat", injson_type))
            sym.type |= OSSA_SYM_CHAT;
        else if(!strcmp("plugin", injson_type))
            sym.type |= OSSA_SYM_PLUGIN;
        else{
            //Search if it's defined custom type
            struct ossaSymbol ct = getFromHeap(injson_type);
            if(ct.type & OSSA_SYM_TYPE){
                sym.type = OSSA_SYM_STRUCTURE;
                sym.data = malloc(strlen(injson_type));
                strcpy(sym.data, injson_type);
            }else{
                sym.type = OSSA_SYM_UNKNOWN;
            }
        }
    }
    return sym;
}
int removeFromHeap(char *symname){
    struct ossaSymbol *iter = heap;
    while(1){
        if(iter->next == 0x0){
            return 0;
        }else{
            if(!strcmp(iter->next->name, symname)){
                void *ptr = iter->next->next;
                free(iter->next);
                iter->next = ptr;
                return 1;
            }
        }
    }
}
extern struct ossaSymbol getFromHeap(char *symname){
    for(struct ossaSymbol *iter = heap; iter!=0x0; iter=iter->next){
        if(iter->name == 0x0) continue;
        if(!strcmp(iter->name, symname)){
            return *iter;
        }
    }
    return (struct ossaSymbol){0x0,0x0,0x0};
}
extern void* initHeap(struct ossaSymbol *heapStorage){
    if(heapStorage == 0x0) {
        heap = malloc(sizeof (struct ossaSymbol));
        *heap = (struct ossaSymbol){0x0,0x0,0x0};
    }
    else heap = heapStorage;
    return heap;
}