//
// Created by rakurs on 5/1/24.
//
#include "./heap.c"

int main(){
    initHeap(0x0);
    struct ossaSymbol sym;
    sym = compileType("{\"name\":\"plugin_repo\",\"type\":\"struct\",\"members\":[{\"name\":\"url\",\"type\":\"string\"},{\"name\":\"plugins_list\",\"type\":\"string\"}]}");
    pushToHeap(sym.type, sym.name, sym.data);
    sym = compileType("{\"name\":\"addRepo\",\"type\":\"function\",\"arguments\":[{\"name\":\"repo\",\"type\":\"plugin_repo\"}]}");
    return 0;
}