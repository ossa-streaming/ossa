//
// Created by rakurs on 5/1/24.
//

#ifndef OSSA_HEAP_H
#define OSSA_HEAP_H

#endif //OSSA_HEAP_H

extern void *getHeapPointer();
extern struct ossaSymbol getFromHeap(char *symname);
extern struct ossaSymbol compileType(char *description, void *exec);
extern int pushToHeap(char type, char *symname, void *data);
extern int removeFromHeap(char *symname);
extern void* initHeap(struct ossaSymbol *heapStorage);
